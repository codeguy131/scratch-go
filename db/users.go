package db

import (
	"github.com/google/uuid"
	"gitlab.com/codeguy131/scratch-go/gql/graph/model"
)

// Database mapping for generated user struct from gqlgen
type User struct {
	model.User `gorm:"embedded;primary_key:not null"`
}

// Initialize new user with default values
func NewUser() *User {
	user := &User{}
	user.ID = uuid.NewString()
	return user
}

// Creates a new user entry in database
func (self *User) Save() error {

	// Connect to and/or initialize database
	conn, err := InitDB()
	if err != nil {
		return err
	}

	if err = conn.Create(self).Error; err != nil {
		return err
	}

	return nil
}

// Load user info
func (self *User) Get() (*User, error) {
	conn, err := InitDB()
	if err != nil {
		return nil, err
	}

	conn.Find(&User{}, "username = ?", self.Username).Scan(self)

	return self, nil

}

// Delete user from users table
func (self *User) Delete() error {

	conn, err := InitDB()
	if err != nil {
		return err
	}

	conn.Where("username = ?", self.Username).Delete(&User{})

	return nil
}
