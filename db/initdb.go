// Generated gqlgen models treated as single source of truth
// for db mapping to gorm
package db

import (
	"fmt"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// Initailize database if not done already
func InitDB() (*gorm.DB, error) {

	// Connection string comes from environment variable
	conn_str := os.Getenv("PG_URL")

	// Postgres config options
	config := postgres.Config{
		DSN:        conn_str,
		DriverName: "pgx",
	}

	// Connect to database
	conn, err := gorm.Open(postgres.New(config))
	if err != nil {
		return nil, err
	}

	// Create tables if not already done
	if !conn.Migrator().HasTable(&User{}) {
		fmt.Printf("No User table yet. Will make now...")
		if err = conn.Migrator().CreateTable(&User{}); err != nil {
			return nil, err
		}

		// Add constraints as needed
		if err = conn.Exec(`                                                                                          
            ALTER TABLE users
            ADD CONSTRAINT idx_unique UNIQUE
            (username);

            ALTER TABLE users
            ADD CONSTRAINT idx_blank_entry 
            CHECK (id <> '' AND account_type <> '' AND username <> '' AND password <> '');
        `).Error; err != nil {
			return nil, err
		}

		conn.AutoMigrate()

	}

	return conn, nil
}
