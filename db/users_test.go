package db_test

import (
	"testing"

	"gitlab.com/codeguy131/scratch-go/db"
	"gitlab.com/codeguy131/scratch-go/gql/graph/model"
)

var test_user = &db.User{
	model.User{
		ID:          "8738b3fc-7f94-4163-8999-9e102b72645e",
		Username:    "test_user",
		Password:    "password123",
		AccountType: model.AccountTypeCustomer,
	},
}

func Test_User_Create_Success(tst *testing.T) {
	if err := test_user.Save(); err != nil {
		tst.Fatal(err)
	}

	test_user.Delete()
	return
}

func Test_Get_User(tst *testing.T) {
	test_user.Save()
	tu_id := test_user.ID
	user := &db.User{}
	user.Username = test_user.Username
	user, err := user.Get()
	if err != nil {
		panic(err)
	}
	if user.ID != tu_id {
		tst.Fatal(user.AccountType)
	}
	test_user.Delete()
	return
}
