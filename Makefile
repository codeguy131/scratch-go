ROOT_DIR=$(PWD)
GQL_DIR=${ROOT_DIR}/gql
BUILD_DIR=${ROOT_DIR}/build
SRV_EXEC=${BUILD_DIR}/server
ENV_FILE=${ROOT_DIR}/.env

build:
	if [[ ! -d ${BUILD_DIR} ]]; then mkdir ${BUILD_DIR}; fi
	go build -o ${SRV_EXEC} .

clean:
	rm -rf ${BUILD_DIR}
	go clean -cache

schema:
	go run github.com/99designs/gqlgen generate

playground:
	${SRV_EXEC}

test:
	go clean -cache
	go test ./...
