# A GraphQL based mock Ecommerce API

## Dependencies

- **[gqlgen](https://github.com/99designs/gqlgen#readme)** _for schema_
- **[gorm](https://gorm.io/index.html)** _for db mappings_
- **[postgresql](https://www.postgresql.org/)** _Database_

## Build

_Can use docker compose for postgres_

- Put environment variables in a .env file in the project root
- Run `docker-compose up --build -d`

## Test
*Using go test*
- Run `go test ./...`
---
*Using Make*
- Run `make test`

## GraphQL playground

- `make playground` or `go run ./server.go`
