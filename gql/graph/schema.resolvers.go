package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/codeguy131/scratch-go/db"
	"gitlab.com/codeguy131/scratch-go/gql/graph/generated"
	"gitlab.com/codeguy131/scratch-go/gql/graph/model"
)

func (r *mutationResolver) AddUser(ctx context.Context, input model.UserInput) (*model.User, error) {
	var user = db.NewUser()
	user.Username = input.Username
	user.Password = input.Password
	user.AccountType = model.AccountTypeCustomer
	if err := user.Save(); err != nil {
		panic(err)
	}
	return &user.User, nil
}

func (r *mutationResolver) DelUser(ctx context.Context, input string) (string, error) {
	var user = &db.User{
		User: model.User{
			Username: input,
		},
	}
	if err := user.Delete(); err != nil {
		return "", err
	}
	return "User deleted successfully", nil
}

func (r *mutationResolver) CreateOrder(ctx context.Context, input model.OrderInput) (string, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Orders(ctx context.Context) ([]*model.Order, error) {
	panic(fmt.Errorf("not implemented"))
}

func (r *queryResolver) Products(ctx context.Context) ([]*model.Product, error) {
	panic(fmt.Errorf("not implemented"))
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
